package com.afs.tdd;

import java.util.List;

/**
 * @Description:
 * @Author: Eisen Chen
 * @CreateDate: 7/13/2023
 **/
public class MarsRover {

    private final int[][] direction = new int[][] {
            {0,1},{1,0},{0,-1},{-1,0}
    };

    private Location location;

    public MarsRover(Location location) {
        this.location = location;
    }

    public void executeCommand(Command command) {
        if (command.equals(Command.Move)) {
            move();
        } else if (command.equals(Command.Left)) {
            turnLeft();
        } else if (command.equals(Command.Right)) {
            turnRight();
        }
    }

    public void executeBatchCommands(List<Command> commands) {
        commands.stream().forEach(command -> executeCommand(command));
    }

    private void turnRight() {
        int index = getIndex(location.getDirection());
        location.setDirection(Direction.values()[(index+1)%4]);
    }

    private void turnLeft() {
        int index = getIndex(location.getDirection());
        location.setDirection(Direction.values()[(index-1+4)%4]);
    }

    private void move() {
        int index = getIndex(location.getDirection());
        location.setCoordinateX(direction[index][0] + location.getCoordinateX());
        location.setCoordinateY(direction[index][1] + location.getCoordinateY());
    }

    public Location getLocation() {
        return location;
    }

    public int getIndex(Direction dir) {
        for (int i = 0;i < 4;++i) {
            if (dir.equals(Direction.values()[i])) {
                return i;
            }
        }
        return 0;
    }
}
