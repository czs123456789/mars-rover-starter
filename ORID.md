Objective: I learned what Testing is. At the same time, we reviewed the code with each about Testing. I learned what TDD (Test Driven Development) is.

Reflective:  Fulfilled. I learned more about Testing.

Interpretive: Testing and TDD can reduce the maintenance costs of the project.

Decisional: We can use testing to reduce program bugs and achieve program accuracy and robustness. The development process is more organized and purposeful allowing for minimal module development and able to test the functionality of the code in a timely manner.